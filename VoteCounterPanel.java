import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class VoteCounterPanel extends JPanel {
    private int votesForJoe;
    private int votesFormary;
    
    private JButton joe;
    private JLabel labelJoe;
    private JPanel joePanel;
    
    private JButton mary;
    private JLabel labelmary;
    private JPanel maryPanel;
    
    private JPanel winnerPanel;
    private JLabel winnerLabel;
    final static private String TIED = "Joe and Mary are tied!";
    final static private String JOE_AHEAD = "Joe is ahead in the vote count!";
    final static private String mary_AHEAD = "Mary is ahead in the vote count!";
    
    
    
    //----------------------------------------------
    // Constructor: Sets up the GUI.
    //----------------------------------------------
    public VoteCounterPanel() {
        votesForJoe = 0;
        votesFormary = 0;
        
        // set layout vertically
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
    
        joePanel = new JPanel();  // horizontal layout
        joe = new JButton("Vote for Joe");
        joe.addActionListener(new combinedButtonListener());
        labelJoe = new JLabel("Votes for Joe: " + votesForJoe);
        joePanel.add(joe);
        joePanel.add(labelJoe);
        joePanel.setOpaque(false);  // transparent JPanel
        add(joePanel);
    
        maryPanel = new JPanel();  // horizontal layout
        mary = new JButton("Vote for mary");
        mary.addActionListener(new combinedButtonListener());
        labelmary = new JLabel("Votes for mary: " + votesFormary);
        maryPanel.add(mary);
        maryPanel.add(labelmary);
        maryPanel.setOpaque(false);  // transparent JPanel
        add(maryPanel);
    
        winnerPanel = new JPanel();  // horizontal layout
        winnerLabel = new JLabel(TIED);
        winnerPanel.add(winnerLabel);
        winnerPanel.setOpaque(false);  // transparent JPanel
        add(winnerPanel);
    
        setPreferredSize(new Dimension(400, 120));
        setMinimumSize(new Dimension(400, 100));
        setBackground(Color.cyan);
    }
    
    
    // updates the text of winnerLabel according to the voting result
    private void updateWinnerLabel(){
        if (votesForJoe > votesFormary){
            winnerLabel.setText(JOE_AHEAD);
        }else if (votesForJoe < votesFormary){
            winnerLabel.setText(mary_AHEAD);
        }else{
            winnerLabel.setText(TIED);
        }
    }
    
    
    private class combinedButtonListener implements ActionListener {
        //----------------------------------------------
        // Updates the counter and label when vote button is pushed
        //----------------------------------------------
        public void actionPerformed(ActionEvent event) {
            if (event.getSource() == joe) {
                votesForJoe++;
                labelJoe.setText("Votes for Joe: " + votesForJoe);
                updateWinnerLabel();
            }else{
                votesFormary++;
                labelmary.setText("Votes for Joe: " + votesFormary);
                updateWinnerLabel();
            }
        }
    }
   
}